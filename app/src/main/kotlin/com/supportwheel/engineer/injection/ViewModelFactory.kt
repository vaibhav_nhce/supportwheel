package com.supportwheel.engineer.injection

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.persistence.room.Room
import android.support.v7.app.AppCompatActivity
import com.supportwheel.engineer.model.database.AppDatabase
import com.supportwheel.engineer.ui.engineer.EngineerListViewModel

class ViewModelFactory(private val activity: AppCompatActivity): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EngineerListViewModel::class.java)) {
            val db = Room.databaseBuilder(activity.applicationContext, AppDatabase::class.java, "engineers").build()
            @Suppress("UNCHECKED_CAST")
            return EngineerListViewModel(db.engineerDao()) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")

    }
}