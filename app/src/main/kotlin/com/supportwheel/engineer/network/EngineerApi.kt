package com.supportwheel.engineer.network

import io.reactivex.Observable
import com.supportwheel.engineer.model.Engineer
import retrofit2.http.GET

/**
 * The interface which provides methods to get result of webservices
 */
interface EngineerApi {
    /**
     * Get the list of the engineers from the API
     */
    @GET("/api/engineers/list/all")
    fun getEngineers(): Observable<List<Engineer>>
}