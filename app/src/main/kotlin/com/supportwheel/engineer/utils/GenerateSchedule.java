package com.supportwheel.engineer.utils;

import com.supportwheel.engineer.model.EngineerData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GenerateSchedule {

    private List<Number> indexList = new ArrayList<Number>();
    void GenerateSchedule(){ }

    public  List<String> getSchedule(List<EngineerData> engineerList) {
        List<Number> numbers = new ArrayList<Number>();
        for (EngineerData engineer : engineerList){
            numbers.add(Integer.parseInt(engineer.getId()));
        }
        indexList.clear();
        generateRandomIndex(numbers);
        //System.out.println("Random -> indexList : " + indexList);
        List<String> weeksDays = new CalendarUtils().getCalendar();
        //System.out.println("Coming Week Days : " + weeksDays);

        List<String> scheduleData = new ArrayList<String>();
        int totalRows = 10, i =10, j=0, counter=0;
        scheduleData.add("");
        scheduleData.add("Morning");
        scheduleData.add("Night");
        while(counter < totalRows) {
            scheduleData.add(weeksDays.get(counter));
            scheduleData.add(engineerList.get(indexList.get(j).intValue()).getName());
            j++;
            scheduleData.add(engineerList.get(indexList.get(j).intValue()).getName());
            j++; i++;
            if(counter == 4) {
                i=0;
                j=0;
            }
            counter++;
        }
        return scheduleData;
    }

    private void generateRandomIndex(List<Number> numbers) {
        while(numbers.size() > 0) {
            Collections.shuffle(numbers);
            indexList.add(numbers.get(0));
            numbers.remove(numbers.get(0));
            generateRandomIndex(numbers);
        }
    }
}
