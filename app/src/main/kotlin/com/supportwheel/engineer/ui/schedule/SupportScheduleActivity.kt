package com.supportwheel.engineer.ui.schedule

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import kotlinx.android.synthetic.main.activity_support_schedule.*

import android.view.ViewGroup
import android.widget.Button
import android.widget.TableLayout
import android.widget.TableRow
import com.supportwheel.engineer.R
import com.supportwheel.engineer.model.Engineer
import com.supportwheel.engineer.model.EngineerData
import com.supportwheel.engineer.utils.GenerateSchedule
import com.supportwheel.engineer.utils.MESSAGE_ENGINEER_LIST
import java.util.*


class SupportScheduleActivity : AppCompatActivity() {

    val tableLayout by lazy { TableLayout(this) }

    private lateinit var engineerList:List<Engineer>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_support_schedule)

        var intent: Intent =intent
        var engineerList:List<EngineerData> = ArrayList<EngineerData>()
        engineerList=intent.getParcelableArrayListExtra(MESSAGE_ENGINEER_LIST)

        val lp = TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        tableLayout.apply {
            layoutParams = lp
            isShrinkAllColumns = true
        }
        var generateSchedule:GenerateSchedule = GenerateSchedule()
        createTable(11, 3, generateSchedule.getSchedule(engineerList));
    }


    fun createTable(rows: Int, cols: Int, result:List<String>) {
        var index : Int = 0
        for (i in 0 until rows) {
            val row = TableRow(this)
            row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)

            for (j in 0 until cols) {
                val button = Button(this)
                button.apply {
                    layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                            TableRow.LayoutParams.WRAP_CONTENT)
                    text = result.get(index)
                    index++;
                }
                button.isClickable = false
                row.addView(button)
            }
            tableLayout.addView(row)
        }
        linearLayout.addView(tableLayout)
    }
}
