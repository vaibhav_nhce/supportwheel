package com.supportwheel.engineer.ui.engineer

import android.arch.lifecycle.MutableLiveData
import android.content.Intent
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import com.supportwheel.engineer.R
import com.supportwheel.engineer.base.BaseViewModel
import com.supportwheel.engineer.model.Engineer
import com.supportwheel.engineer.model.EngineerDao
import com.supportwheel.engineer.network.EngineerApi
import com.supportwheel.engineer.utils.GenerateSchedule
import java.time.DayOfWeek
import java.util.*
import javax.inject.Inject

class EngineerListViewModel(private val engineerDao: EngineerDao):BaseViewModel(){
    @Inject
    lateinit var engineerApi: EngineerApi
    val engineerListAdapter: EngineerListAdapter = EngineerListAdapter()

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage:MutableLiveData<Int> = MutableLiveData()
    val generateScheduleButton:MutableLiveData<String> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadEngineers() }
    val generateSchedule = View.OnClickListener { onClickGenerateSchedule() }

    private lateinit var subscription: Disposable

    init{
        loadEngineers()
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun loadEngineers(){
        subscription = Observable.fromCallable { engineerDao.all }
                .concatMap {
                    dbEngineerList ->
                        if(dbEngineerList.isEmpty())
                            engineerApi.getEngineers().concatMap {
                                            apiEngineerList -> engineerDao.insertAll(*apiEngineerList.toTypedArray())
                                 Observable.just(apiEngineerList)
                                       }
                        else
                            Observable.just(dbEngineerList)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrieveEngineerListStart() }
                .doOnTerminate { onRetrieveEngineerListFinish() }
                .subscribe(
                        { result -> onRetrieveEngineerListSuccess(result) },
                        { onRetrieveEngineerListError() }
                )
    }

    private fun onRetrieveEngineerListStart(){
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrieveEngineerListFinish(){
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveEngineerListSuccess(engineerList:List<Engineer>){
        engineerListAdapter.updateEngineerList(engineerList)
    }

    private fun onRetrieveEngineerListError(){
        errorMessage.value = R.string.post_error
    }

    private fun onClickGenerateSchedule(){
        loadingVisibility.value = View.VISIBLE
        generateScheduleButton.value = "onClicked"
    }

}