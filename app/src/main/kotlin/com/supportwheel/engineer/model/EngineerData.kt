package com.supportwheel.engineer.model

import android.os.Parcel
import android.os.Parcelable

class EngineerData(val id:String,val name:String) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<EngineerData> {
            override fun createFromParcel(parcel: Parcel): EngineerData {
                return EngineerData(parcel)
            }

            override fun newArray(size: Int): Array<EngineerData?> {
                return arrayOfNulls(size)
            }
        }
    }
}

