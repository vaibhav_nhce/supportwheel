package com.supportwheel.engineer.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Class which provides a model for engineer
 * @constructor Sets all properties of the engineer
 * @property id the unique identifier of the engineer
 * @property name the title of the engineer
 */
@Entity
data class Engineer(
        @field:PrimaryKey
        val id: String,
        val name: String
)
