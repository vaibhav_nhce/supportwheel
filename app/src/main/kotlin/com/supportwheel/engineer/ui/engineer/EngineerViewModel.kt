package com.supportwheel.engineer.ui.engineer

import android.arch.lifecycle.MutableLiveData
import com.supportwheel.engineer.base.BaseViewModel
import com.supportwheel.engineer.model.Engineer

class EngineerViewModel:BaseViewModel() {
    private val id = MutableLiveData<String>()
    private val name = MutableLiveData<String>()

    fun bind(engineer: Engineer){
        id.value = engineer.id
        name.value = engineer.name
    }

    fun getEngineerId():MutableLiveData<String>{
        return id
    }

    fun getEngineerName():MutableLiveData<String>{
        return name
    }

}