# Support Wheel of Fate
 - MVVM architecture with Kotlin/Java
 - Dagger 2
 - Retrofit 
 - Room
 - RxAndroid
 
 
 To create a mock API, Apiary Service has been used.
 
 http://private-609f04-supportwheel.apiary-mock.com/api/engineers/list/all

