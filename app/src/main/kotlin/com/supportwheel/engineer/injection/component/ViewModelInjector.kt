package com.supportwheel.engineer.injection.component

import dagger.Component
import com.supportwheel.engineer.injection.module.NetworkModule
import com.supportwheel.engineer.ui.engineer.EngineerListViewModel
import com.supportwheel.engineer.ui.engineer.EngineerViewModel

import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified PostListViewModel.
     * @param engineerListViewModel EngineerListViewModel in which to inject the dependencies
     */
    fun inject(engineerListViewModel: EngineerListViewModel)
    /**
     * Injects required dependencies into the specified PostViewModel.
     * @param engineerViewModel EngineerViewModel in which to inject the dependencies
     */
    fun inject(engineerViewModel: EngineerViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}