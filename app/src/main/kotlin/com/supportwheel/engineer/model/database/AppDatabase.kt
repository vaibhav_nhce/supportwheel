package com.supportwheel.engineer.model.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.supportwheel.engineer.model.Engineer
import com.supportwheel.engineer.model.EngineerDao


@Database(entities = [Engineer::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun engineerDao(): EngineerDao
}