package com.supportwheel.engineer.utils

/** The base URL of the API */
//const val BASE_URL: String = "https://jsonplaceholder.typicode.com"

const val BASE_URL: String = "http://private-609f04-supportwheel.apiary-mock.com"
const val MESSAGE_ENGINEER_LIST = "engineerList"
