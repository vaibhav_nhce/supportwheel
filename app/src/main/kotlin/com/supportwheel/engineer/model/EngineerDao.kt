package com.supportwheel.engineer.model

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface EngineerDao {
    @get:Query("SELECT * FROM engineer")
    val all: List<Engineer>

    @Insert
    fun insertAll(vararg users: Engineer)
}