package com.supportwheel.engineer.ui.engineer

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.supportwheel.engineer.R
import com.supportwheel.engineer.databinding.ActivityEngineerListBinding
import com.supportwheel.engineer.injection.ViewModelFactory
import android.content.Intent
import android.os.Parcelable
import android.view.View
import com.supportwheel.engineer.ui.schedule.SupportScheduleActivity
import com.supportwheel.engineer.utils.MESSAGE_ENGINEER_LIST



class EngineerListActivity: AppCompatActivity() {
    private lateinit var binding: ActivityEngineerListBinding
    private lateinit var viewModel: EngineerListViewModel
    private var errorSnackbar: Snackbar? = null


    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_engineer_list)
        binding.engineerList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        viewModel = ViewModelProviders.of(this, ViewModelFactory(this)).get(EngineerListViewModel::class.java)
        viewModel.errorMessage.observe(this, Observer {
            errorMessage -> if(errorMessage != null) showError(errorMessage) else hideError()
        })

        viewModel.generateScheduleButton.observe(this, Observer {
            startSupportScheduleActivity()
        })

        binding.viewModel = viewModel
    }

    private fun showError(@StringRes errorMessage:Int){
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError(){
        errorSnackbar?.dismiss()
    }

    private fun startSupportScheduleActivity(){
        var intent:Intent=Intent(applicationContext,SupportScheduleActivity::class.java)
        intent.putParcelableArrayListExtra(MESSAGE_ENGINEER_LIST ,viewModel.engineerListAdapter.getEngineerList() as java.util.ArrayList<out Parcelable>)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadingVisibility.value = View.GONE
    }

}