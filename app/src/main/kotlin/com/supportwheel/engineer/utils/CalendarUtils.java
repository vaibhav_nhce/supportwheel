package com.supportwheel.engineer.utils;

import java.text.SimpleDateFormat;
import java.util.*;


public class CalendarUtils {
    private Calendar c;
    private List<String> result;
    private SimpleDateFormat fmt = new SimpleDateFormat("EEE, d MMM yyyy");

    public CalendarUtils() {
        c = Calendar.getInstance();
        result =  new ArrayList<String>();
    }

    public List<String> getCalendar() {
        //Get current Day of week and Apply suitable offset to bring the new calendar
        //back to the appropriate Monday, i.e. this week or next

        switch (c.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.SUNDAY:
                c.add(Calendar.DATE, 1);
                break;

            case Calendar.MONDAY:
                //Don't need to do anything on a Monday
                //included only for completeness
                break;

            case Calendar.TUESDAY:
                c.add(Calendar.DATE,-1);
                break;

            case Calendar.WEDNESDAY:
                c.add(Calendar.DATE, -2);
                break;

            case Calendar.THURSDAY:
                c.add(Calendar.DATE,-3);
                break;

            case Calendar.FRIDAY:
                c.add(Calendar.DATE,-4);
                break;

            case Calendar.SATURDAY:
                c.add(Calendar.DATE,2);
                break;
        }

        //Add the Monday to the result
        result.add(fmt.format(c.getTime()));
        for (int x = 1; x <14; x++) {
            //Add the remaining days to the result
            c.add(Calendar.DATE,1);
            if(x > 0 && x < 5 || x > 6 && x < 12) {
                result.add(fmt.format(c.getTime()));
            }
        }
        return this.result;
    }
}
