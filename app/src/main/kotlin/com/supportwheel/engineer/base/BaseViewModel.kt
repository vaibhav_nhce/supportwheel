package com.supportwheel.engineer.base

import android.arch.lifecycle.ViewModel
import com.supportwheel.engineer.injection.component.DaggerViewModelInjector
import com.supportwheel.engineer.injection.component.ViewModelInjector
import com.supportwheel.engineer.injection.module.NetworkModule
import com.supportwheel.engineer.ui.engineer.EngineerListViewModel
import com.supportwheel.engineer.ui.engineer.EngineerViewModel

abstract class BaseViewModel:ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is EngineerListViewModel -> injector.inject(this)
            is EngineerViewModel -> injector.inject(this)
        }
    }
}