package com.supportwheel.engineer.ui.engineer

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.adapters.ViewBindingAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import com.supportwheel.engineer.R
import com.supportwheel.engineer.databinding.ItemEngineerBinding
import com.supportwheel.engineer.model.Engineer
import com.supportwheel.engineer.model.EngineerData


class EngineerListAdapter: RecyclerView.Adapter<EngineerListAdapter.ViewHolder>() {
    private lateinit var engineerList:List<Engineer>
    lateinit var listener: OnItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EngineerListAdapter.ViewHolder {
        val binding: ItemEngineerBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_engineer, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: EngineerListAdapter.ViewHolder, position: Int) {
        holder.bind(engineerList[position])
    }

    override fun getItemCount(): Int {
        return if(::engineerList.isInitialized) engineerList.size else 0
    }

    fun updateEngineerList(engineerList:List<Engineer>){
        this.engineerList = engineerList
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onClick(view: View, data: EngineerViewModel)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.listener =  listener
    }

    fun getEngineerList(): List<EngineerData> {
        var data:MutableList<EngineerData> = ArrayList<EngineerData>()
        for (i in engineerList.indices) {
            var engineerData: EngineerData = EngineerData(engineerList[i].id,engineerList[i].name)
            data.add(engineerData)
        }
        return data;
    }

    class ViewHolder(private val binding: ItemEngineerBinding):RecyclerView.ViewHolder(binding.root){
        private val viewModel = EngineerViewModel()
        fun bind(engineer:Engineer){
            viewModel.bind(engineer)
            binding.viewModel = viewModel
        }
    }
}